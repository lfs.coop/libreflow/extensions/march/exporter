import os
import re
import shutil
import gazu
import subprocess
from kabaret import flow
from datetime import datetime, date
from libreflow.utils.flow.values import MultiOSParam
from libreflow.utils.os import remove_folder_content


def copytree_no_metadata(src, dst):
    '''
    Recursively copy an entire directory tree rooted at `src`
    to a directory named `dst`, without copying metadata.
    '''
    if not os.path.isdir(dst):
        os.mkdir(dst)
    
    for f in os.scandir(src):
        if f.is_dir():
            copytree_no_metadata(f.path, os.path.join(dst, f.name))
        else:
            shutil.copyfile(f.path, os.path.join(dst, f.name))


class ExportSettings(flow.Object):

    root_folder = MultiOSParam().ui(editable=False)
    skip_kitsu_30_update = flow.Param(list(['sc103C'])).ui(
        label="Skip Kitsu 3.0 update",
        tooltip="Skip status update on Kitsu 3.0 for these sequences (names)")


class ExportShotFiles(flow.Action):

    _delivery_folder = flow.Computed()

    _task_type   = flow.Computed()
    _task_status = flow.Computed()

    _shot = flow.Parent()
    _sequence = flow.Parent(3)

    def needs_dialog(self):
        self._delivery_folder.touch()
        self.message.set(
            '<h3>⚠️ This shot has already been prepared for delivery today.<br><br>Overwrite ?</h3>'
        )
        return os.path.isdir(self._delivery_folder.get())

    def get_buttons(self):
        return ['Confirm', 'Cancel']
    
    def get_latest_available(self, file_name):
        latest = None
        try:
            task = self._shot.tasks['assist_key']
        except flow.exceptions.MappedNameError:
            pass
        else:
            try:
                _file = task.files[file_name]
            except flow.exceptions.MappedNameError:
                pass
            else:
                latest = _file.get_head_revision()
                if latest is not None and not os.path.exists(latest.get_path()):
                    latest = None
        
        return latest
    
    def compute_child_value(self, child_value):
        config = self.root().project().kitsu_config().kitsu_studio_30
        settings = config.get_task_settings('assist_key')

        if child_value is self._delivery_folder:
            settings = self.root().project().admin.project_settings.export_settings
            self._delivery_folder.set(
                os.path.join(
                    settings.root_folder.get(),
                    datetime.now().strftime('%y%m%d'),
                    f'{self._sequence.name()}_{self._shot.name()}'
                )
            )

        if child_value is self._task_type:
            self._task_type.set(settings.get('target_type'))
        elif child_value is self._task_status:
            self._task_status.set(settings.get('target_status'))
    
    def export_files(self, delivery_folder):
        scene = self.get_latest_available('clean_blend')
        refs = self.get_latest_available('refs')
        mov = self.get_latest_available('clean_movie_mov')
        mov_ml = self.get_latest_available('clean_movie_ml_mov')

        print(f'Export :: {self._sequence.name()} {self._shot.name()} -> {delivery_folder}')
        print(f'Export ::   assist key: {scene.name()}')

        if not os.path.isdir(delivery_folder+'/WIP'):
            os.makedirs(delivery_folder+'/WIP', exist_ok=True)
        
        shutil.copyfile(scene.get_path(), delivery_folder+'/WIP/'+os.path.basename(scene.get_path()))

        if mov is not None:
            print(f'Export ::   movie: {mov.name()}')
            os.makedirs(delivery_folder+'/EXPORTS', exist_ok=True)
            shutil.copyfile(mov.get_path(), delivery_folder+'/EXPORTS/'+os.path.basename(mov.get_path()))
        if mov_ml is not None:
            print(f'Export ::   matchlines movie: {mov_ml.name()}')
            os.makedirs(delivery_folder+'/EXPORTS', exist_ok=True)
            shutil.copyfile(mov_ml.get_path(), delivery_folder+'/EXPORTS/'+os.path.basename(mov_ml.get_path()))
        if refs is not None:
            print(f'Export ::   refs: {refs.name()}')
            if os.path.isdir(delivery_folder+'/IMAGES'):
                shutil.rmtree(delivery_folder+'/IMAGES')
            
            copytree_no_metadata(refs.get_path(), delivery_folder+'/IMAGES')
        
        if mov is not None:
            self.set_kitsu_status(mov.get_path(), mov_ml)
        
    def set_kitsu_status(self, preview_path, preview_ml):
        print('Export ::   Switch to DONE status in LFS Kitsu')
        self.root().project().kitsu_api().set_shot_task_status(
            self._sequence.name(),
            self._shot.name(),
            'assist_key',
            'DONE',
            f'Envoyé le {date.today().strftime("%d/%m/%y")} à 3.0'
        )

        if self._sequence.name() in self.root().project().admin.project_settings.export_settings.skip_kitsu_30_update.get():
            print('Export ::   Skip upload to 3.0 Kitsu')
            print('Export ::   Compress movies')
            
            # Convert movies to MP4
            mp4_path = os.path.splitext(preview_path)[0] + '.mp4'
            ffmpeg_path = self.root().project().admin.project_settings.ffmpeg_path.get()
            subprocess.run([
                ffmpeg_path,
                '-loglevel',
                'panic',
                '-y',
                '-i',
                preview_path,
                '-vcodec',
                'libx264',
                '-crf',
                '28',
                mp4_path
            ], shell=True)
            print(f'Export ::     movie: {mp4_path}')

            if preview_ml:
                preview_ml_path = preview_ml.get_path()
                mp4_path = os.path.splitext(preview_ml_path)[0] + '.mp4'
                subprocess.run([
                    ffmpeg_path,
                    '-loglevel',
                    'panic',
                    '-y',
                    '-i',
                    preview_ml_path,
                    '-vcodec',
                    'libx264',
                    '-crf',
                    '28',
                    mp4_path
                ], shell=True)
                print(f'Export ::     matchlines movie: {mp4_path}')

            return
        
        # Switch to 3.0 Studio Kitsu
        print('Export ::   Connect to 3.0 Kitsu')
        store_lfs_user = gazu.client.default_client.tokens
        config = self.root().project().kitsu_config().kitsu_studio_30
        gazu.set_host(config.host.get())
        gazu.log_in(config.login.get(), config.password.get())

        if preview_ml:
            print('Export ::   Upload matchlines playblast to 3.0 Kitsu')
            config.upload_shot_preview(
                self._sequence.name(),
                self._shot.name(),
                self._task_type.get(),
                self._task_status.get(),
                preview_ml.get_path(),
                f"Version validée. Rendu avec ML, envoyé le {date.today().strftime('%y%m%d')}"
            )

        print('Export ::   Upload playblast to 3.0 Kitsu')
        config.upload_shot_preview(
            self._sequence.name(),
            self._shot.name(),
            self._task_type.get(),
            self._task_status.get(),
            preview_path,
            f"Version validée. Envoyé le {date.today().strftime('%y%m%d')}"
        )

        # Go back to LFS Kitsu and restore user token
        print('Export ::   Back to LFS Kitsu')
        gazu.set_host(self.root().project().kitsu_config().server_url.get()+'/api')
        gazu.client.set_tokens(store_lfs_user)
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        scene = self.get_latest_available('clean_blend')

        if scene is None:
            self.root().session().log_warning(
                f'Export :: Clean of shot {self._shot.name()} not started yet'
            )
            return
        
        self.export_files(self._delivery_folder.get())


class ShotNamesValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'multichoice'

    _sequence = flow.Parent(2)

    def choices(self):
        return self._sequence.shots.mapped_names()


class ExportSequenceFiles(flow.Action):

    shots = flow.SessionParam([], ShotNamesValue)
    select_all = flow.SessionParam(False).ui(editor='bool').watched()
    skip_existing = flow.SessionParam(True).ui(editor='bool')

    _delivery_folder = flow.Computed()

    _sequence = flow.Parent()

    def needs_dialog(self):
        self.message.set('<h2>Export shot files</h2>')
        return True

    def get_buttons(self):
        return ['Export', 'Cancel']
    
    def compute_child_value(self, child_value):
        if child_value is self._delivery_folder:
            settings = self.root().project().admin.project_settings.export_settings
            self._delivery_folder.set(
                os.path.join(
                    settings.root_folder.get(),
                    datetime.now().strftime('%y%m%d')
                )
            )
    
    def child_value_changed(self, child_value):
        if child_value is self.select_all:
            self.shots.set(self.shots.choices() if self.select_all.get() else [])

    def run(self, button):
        if button == 'Cancel':
            return
        
        for n in self.shots.get():
            shot = self._sequence.shots[n]
            shot_delivery_folder = self._delivery_folder.get()+f'/{self._sequence.name()}_{shot.name()}'

            if self.skip_existing.get() and os.path.isdir(shot_delivery_folder):
                print(f'Export :: {self._sequence.name()} {shot.name()} already processed')
                continue

            a = shot.export_files
            scene = a.get_latest_available('clean_blend')

            if scene is not None:
                a.export_files(shot_delivery_folder)


def export_settings(parent):
    if re.match('^/march/admin/project_settings$', parent.oid()):
        r = flow.Child(ExportSettings)
        r.name = 'export_settings'
        r.index = None
        return r


def export_sequence_files(parent):
    if re.match('^/march/films/march(_test)?/sequences/sc\d{3}\D?$', parent.oid()):
        r = flow.Child(ExportSequenceFiles)
        r.name = 'export_files'
        r.index = None
        return r


def export_shot_files(parent):
    if re.match('^/march/films/march(_test)?/sequences/sc\d{3}\D?/shots/(sh\d{4}|fig\d+\D?)$', parent.oid()):
        r = flow.Child(ExportShotFiles)
        r.name = 'export_files'
        r.index = None
        return r


def install_extensions(session): 
    return {
        "march": [
            export_settings,
            export_sequence_files,
            export_shot_files
        ],
    }
